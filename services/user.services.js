const User = require("../model/User");


class UserServices {

    async saveUser(username, password, email, name) {
        try {
            const user = new User({
                username: username,
                password: password,
                email: email,
                name: name
            });

            const savedUser = await user.save();
            return savedUser;
        }
        catch (error) {
            return error;
        }
    }


    async getUsers() {
        const userData = await User.find({}, { _id: 0, "password": 0, "__v": 0, "date": 0 });
        return userData;
    }

    async updateUsers(username, name, password, email) {
        const updatedData = await User.updateOne({ username: username },
            {
                $set: {
                    name: name,
                    email: email,
                    password: password
                }
            })

        return updatedData;
    }

    async getUsersByUname(username) {
        const userData = await User.find({ username: username });
        return userData;
    }

    async checkUserExistById(id) {
        const userData = await User.find({ _id: id });
        return userData;
    }

    async resetPassDb(id, password) {
        const reset = await User.updateOne({ _id: id }, { $set: { password: password } });
        return reset;
    }

    async deleteUsersDb(id) {
        console.log(id)
        const delUser = await User.remove({ _id: id });
        return delUser;
    }
}


module.exports = new UserServices;