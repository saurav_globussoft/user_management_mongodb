const Joi = require("joi");

const schema = Joi.object({
    username: Joi.string().min(6).required(),
    password: Joi.string().min(6).required(),
    name: Joi.string().min(2).required(),
    email: Joi.string().min(6).required().email()
});


module.exports = { schema };


