const router = require("express").Router();

const MainController = require("../controller/main.controller");

const { authenticateToken, authenticateTokenEmail } = require("../middleware/auth.middleware");

router.post("/register", MainController.createUserData);

router.post("/user/login", MainController.userLogin);

router.get("/user/verify", MainController.verifyEmail);

router.put("/user/reset", authenticateTokenEmail, MainController.resetPassword);

router.use(authenticateToken);

router.get("/user", MainController.getUserDetails);

router.put("/user", MainController.updateUserData);

router.delete("/user", MainController.deleteUserData);

module.exports = router;