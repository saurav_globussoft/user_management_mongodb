// dependencies included
const express = require("express");

const app = express();

const dotenv = require('dotenv').config();

const connectDb = require("./mongoose_conn/connection");

connectDb();

let PORT = process.env.PORT || 8000;

app.use(express.json());

//main route setup  
const mainRouter = require("./routes/mainroute");

//express route setup
app.use(mainRouter);

//app config to a port
app.listen(PORT, () => {
    console.log(`Listening to port ${PORT}`);
});