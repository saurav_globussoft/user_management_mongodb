const mongoose = require("mongoose");

//connecting to database
async function connectDb() {
    try {
        const connection = await mongoose.connect('mongodb://localhost:27017/user_manag', { useNewUrlParser: true, useUnifiedTopology: true  });
        console.log("connection established to DB...");
    } catch (error) {
        console.log(error);
    }
}

module.exports = connectDb;