const userServices = require("../services/user.services");
const { createToken } = require("../utils/jwt.util");
const { schema } = require("../services/user.validate")
const { sendEmailLogin, sendEmailVerify } = require("../services/email.services");
const encryptDecrypt = require("../services/encryptdecrypt.services")

class MainController {

    async createUserData(req, res) {
        try {
            const { username, password, email, name } = req.body;

            const validator = await schema.validateAsync(req.body);

            const [userData] = await userServices.getUsersByUname(username);
            if (userData) return res.status(405).json({ code: 405, message: "Username already used" });

            const pass = await encryptDecrypt.encryptPass(password);

            const saveUser = await userServices.saveUser(username, pass, email, name);

            const aEmail = await sendEmailLogin(username, email, password);

            res.status(201).json({ code: 201, message: "New User Created" });
        }
        catch (error) {
            if (error.isJoi === true) {
                res.status(405).json(error.details[0].message)
            }
            res.status(404).json(error);
        }
    }


    /**
     * userLogin - function to login user 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async userLogin(req, res) {
        try {
            const { username, password } = req.body;
            const [userData] = await userServices.getUsersByUname(username);
            if (!userData) return res.status(404).json({ code: 404, message: "Username not found" });

            const decrptPass = await encryptDecrypt.decryptPass(password, userData.password);

            const token = createToken(userData._id);
            res.status(200).json({ code: 200, access_token: token });

        } catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * getUserDetails - function to get user details
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async getUserDetails(req, res) {
        const userDataDB = await userServices.getUsers();
        return res.status(200).json({ code: 200, data: userDataDB });
    }



    /**
     * updateUserData - function to update user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async updateUserData(req, res) {
        try {
            const { id } = req.user;
            const { username, name, password, email } = req.body;

            const [userData] = await userServices.getUsersByUname(username);
            if (!userData) return res.status(404).json({ code: 404, message: "Username not found" });
            console.log(userData);

            if (userData.id != id) return res.status(404).json({ code: 404, message: "Username and ID do not match" });

            const pass = await encryptDecrypt.encryptPass(password);

            const updateUserData = await userServices.updateUsers(username, name, pass, email);
            return res.status(200).json({ code: 200, message: "User updated" });
        }
        catch (error) {
            res.status(404).json(error);
        }
    }


    /**
     * verifyEmail - funtion to very username and email
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async verifyEmail(req, res) {
        try {
            const { username } = req.body;

            const [userData] = await userServices.getUsersByUname(username);
            if (!userData) return res.status(404).json({ code: 404, message: "Username not found" });

            console.log(userData.email, userData._id)
            const aEmail = await sendEmailVerify(userData.email, userData._id);

            return res.status(200).json({ code: 200, message: "Link sent to your email successfully" });
        }
        catch (error) {
            return res.status(404).json(error);
        }
    }


    /**
    * resetPassword - function to reset password
    * @param {*} req 
    * @param {*} res 
    * @returns 
    */
    async resetPassword(req, res) {
        try {
            const { id } = req.user;
            const { password1, password2 } = req.body;
            if ((!password1) || (!password2)) return res.status(404).json({ code: 404, message: "Enter password and confirm password" })
            if (password1 !== password2) return res.status(405).json({ code: 405, message: "Confirmation password should be same" });

            const pass = await encryptDecrypt.encryptPass(password1);
            const resetPassDb = await userServices.resetPassDb(id, pass);

            return res.status(205).json({ code: 205, message: "Password changed successfully" });

        } catch (error) {
            return res.status(400).json(error);
        }
    }


    /**
     * deleteUserData - function to delete user
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async deleteUserData(req, res) {
        try {
            const { id } = req.user;
            const deleteUserData = await userServices.deleteUsersDb(id);
            return res.status(200).json({ code: 200, message: "User deleted successfully" })
        } catch (error) {
            res.status(404).json(error);
        }
    }
}

module.exports = new MainController;